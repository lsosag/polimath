import React, { Component } from "react";
import developer from "../Images/developer.png";

export class Calc extends Component {
  render() {
    return (
      <iframe width="560" height="315" src="https://www.solumaths.com/es/calculadora/calcular/computadora" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    );
  }
}
export default Calc;