import React from 'react';

//Estilos
import '../Styles/App.css';

//Componentes
import { NavBar } from './navbar'

const Layout = props => {
    return (
        <React.Fragment>
            <NavBar />
            {props.children}
        </React.Fragment>
    )
}
export default Layout;