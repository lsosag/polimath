import React from "react";
import CardButton from "../Components/CardButton";
import CardButtonone from "../Components/CardButtonone";

function Home() {
  return (
        <React.Fragment className='reactFragment'>
          <div className="Page-header">
            <div className=" d-flex justify-content-center">
              <div className="m-md-3">
                <CardButton
                  Nombre="limite"
                  Descripcion="Concepto de Limite"
                  Boton="Ingresar"
                  Ruta="Derivada"
                />
              </div>
              <div className="m-md-3">
                <CardButton
                  Nombre="Derivada"
                  Descripcion="Concepto de Derivada"
                  Boton="Ingresar"
                  Ruta="Limite"
                />
              </div>
              <div className="m-md-3">
                <CardButton
                  Nombre="Integral"
                  Descripcion="Concepto de Integral"
                  Boton="Ingresar"
                  Ruta="Integral"
                />
              </div>
              <div className="m-md-3">
                <CardButton
                  Nombre="Quiz General"
                  Descripcion="Prueba tus conocimientos"
                  Boton="Comenzar"
                  Ruta="QuizGeneral"
                />
              </div>
              <tr></tr>
            </div>
            <div className=" d-flex justify-content-center">
              <div class="cars" className="m-md-3">
                <CardButtonone
                  Nombre="Ejercicios de limites"
                  Descripcion="Prueba tus conocimientos en los siguientes ejercicios"
                  Boton="Comenzar"
                  Ruta="QuizLimite_preguntas"
                />
              </div>
              <div class="cars" className="m-md-3">
                <CardButtonone
                  Nombre="Ejercicios de integrales"
                  Descripcion="Prueba tus conocimientos en los siguientes ejercicios"
                  Boton="Comenzar"
                  Ruta="QuizIntegral_preguntas"
                />
              </div>

            </div>
          <div class="box">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/RljDSMHH8l4" frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
      </div>
    </React.Fragment>
  );
}
export default Home;
