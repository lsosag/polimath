// import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import RiemannIntegrability from "../Components/Graficas/RiemannIntegrability";
import React, { Component } from 'react';
import ReactPlayer from 'react-player';
import developer from "../Videos/inte.mp4";




class Integral_preguntas extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="Page-header">
          <h1>Guia para entender el concepto de integral</h1>
          <Container fluid="true">
            <Row noGutters="true">
              <Col>
              <hr></hr>
                <div className="overflow">
                <blockquote class="blockquote">
                    <p class="mb-5">En el siguiente Quiz pondremos en practica lo anterior estudiado, para esto tendras que usar lapiz y papel</p>
                    <p class="mb-1">para poder hacer los ejercicios y llegar al resultado final</p> 
               
                </blockquote>
                </div>
              </Col>
            </Row>
          </Container>
          <div style={{ margin: "8px",display: 'flex', justifyContent: 'center'}}>
          <Link to="/QuizIntegral_preguntas">
            <Button
              variant="success"
            >
              Quiz
            </Button>
          </Link>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Integral_preguntas;
