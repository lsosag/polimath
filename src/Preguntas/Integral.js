export const quiz =  {
    "quizTitle": "Integral",
    "quizSynopsis": "Quiz sobre integrales",
    "questions": [
      {
        "question": "¿A que hace referencia el concepto de “divide y venceras”?",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Regla para aplicar la integral",
          "Metodo de integración",
          "Dividir un problema complejo en problemas mas sencillos",
          "Disminuir un problema paulatinamente"
        ],
        "correctAnswer": "3",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Dividir un problema complejo en problemas más sencillos es conocido como el método griego llamado “agotamiento” o “divide y venceras” ",
        "point": "20"
      },
       {
         "question": "Realice el procedimiento de la integración y escoja el resultado final:  ∫x senx x dx",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "-xCosx + Snx + C",
          "-xCos + Snx ",
          "-xcosx + Tnx + C"
        ],
        "correctAnswer": "1",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
         "explanation": "u=x ---derivar---> u'=1” / v'=senx ---integrar---> v=-cosx / ∫ xsenx dx = ∫ cosx dx =  /    =-xCosx + Snx + C",
        "point": "20"
      },
      {
        "question": "Porque surge la integración",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Para hallar el tamaño de un volumen",
          "Se necesitaba calcular areas mas complejas"
        ],
        "correctAnswer": "2",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "En la antiguedad hubo un problema, el problema de las cuadraturas donde se necesitaba saber el area de figuras complejas",
        "point": "20"
      },
      {
        "question": "Para hallar el area de una curva debo de realizar rombos?",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Correcto",
          "Falso"
        ],
        "correctAnswer": "2",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Tienes que dividirla en rectangulos, igual que en la grafica.",
        "point": "10"
      },
      {
        "question": "Cual de los siguientes conceptos hace refencia a la integral",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Es una técnica que consiste en la descomposición en factores de una expresión algebraica en forma de producto.",
          "relación de orden que se da entre dos valores cuando éstos son distintos ",
          "generalización de la suma de infinitos sumandos",
        ],
        "correctAnswer": "3",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "La integración es un concepto fundamental del cálculo y del análisis matemático. Básicamente, una integral es una generalización de la suma de infinitos sumandos, infinitesimalmente pequeños: una suma continua. La integral es la operación inversa a la derivada.",
        "point": "30"
      },
    ]
  } 