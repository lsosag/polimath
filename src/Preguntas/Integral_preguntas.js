export const quiz =  {
    "quizTitle": "Integral_preguntas",
    "quizSynopsis": "Quiz sobre integrales",
    "questions": [
      {
        "question": "La derivada de la integral definida entre [0,x] de la función f(x) = Ln t / t es:",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "1/x",
          "(1 - Ln x)/x²",
          " Ln x / x",
          " Ln x / x²"
        ],
        "correctAnswer": "3",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Excelente!!! lo has logrado ",
        "point": "20"
      },
       {
         "question": "Realice el procedimiento de la integración y escoja el resultado final:  ∫x senx x dx",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "-xCosx + Snx + C",
          "-xCos + Snx ",
          "-xcosx + Tnx + C"
        ],
        "correctAnswer": "1",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
         "explanation": "u=x ---derivar---> u'=1” / v'=senx ---integrar---> v=-cosx / ∫ xsenx dx = ∫ cosx dx =  /    =-xCosx + Snx + C",
        "point": "20"
      },
      {
        "question": "Sea f(x) = 3x + 1, ¿Cuál es su integral definida en [-1,2]?",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "7/4",
          "0",
          "2",
          "15/2"
        ],
        "correctAnswer": "4",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Excelente!!! lo has logrado",
        "point": "20"
      },
      {
        "question": "La integral definida entre [0, pi/4] de la función f(x) = tag x es igual a:",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Ln (cos(pi/4))",
          "- Ln(cos(pi/4)",
          "cos(pi/4)",
          "1/2"
        ],
        "correctAnswer": "2",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Excelente!!! lo has logrado",
        "point": "20"
      },
    ]
  } 