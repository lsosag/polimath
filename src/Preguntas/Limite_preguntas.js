export const quiz =  {
    "quizTitle": "Limite_preguntas",
    "quizSynopsis": "Ejercicios de limites",
    "questions": [
      {
        "question": "1.El valor de lim n->3 (1/6-2x) es:",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "∞",
          "-∞",
          "∞",
          "0"
        ],
        "correctAnswer": "1",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "Felicitaciones lo haz logrado",
        "point": "20"
      },
      {
        "question": "¿los límites son fundamentales para el calculo?",
        "questionType": "text",
        "answerSelectionType": "single",
        "answers": [
          "Falso",
          "Correcto"
        ],
        "correctAnswer": "2",
        "messageForCorrectAnswer": "Respuesta Correcta. Buen Trabajo!",
        "messageForIncorrectAnswer": "Respuesta Incorrecta. Intenta de Nuevo.",
        "explanation": "El estudio del Cálculo Diferencial se inicia tratando el concepto de función, luego la continuidad de una función basado en el concepto de límite",
        "point": "10"
      },
    ]
  } 